import numpy as np
import tensorflow as tf


class CSML:

	def __init__(self, imgsz, imgc, nway, meta_lr=1e-3, train_lr=1e-2):
		"""

		:param d:
		:param c:
		:param nway:
		:param meta_lr:
		:param train_lr:
		"""
		self.imgsz = imgsz
		self.imgc = imgc
		self.nway = nway
		self.meta_lr = meta_lr
		self.train_lr = train_lr

		self.c = 64
		self.d = 5

		print('\nimg shape:', self.imgsz, self.imgsz, self.imgc, 'meta-lr:', meta_lr, 'train-lr:', train_lr)

	def build(self, support_xb, support_yb, query_xb, query_yb, K, meta_batchsz, mode='train'):
		"""

		:param support_xb:   [b, setsz, 84*84*3]
		:param support_yb:   [b, setsz, n-way]
		:param query_xb:     [b, querysz, 84*84*3]
		:param query_yb:     [b, querysz, n-way]
		:param K:           train update steps
		:param meta_batchsz:tasks number
		:param mode:        train/eval/test, for training, we build train&eval network meanwhile.
		:return:
		"""
		# create or reuse network variable, not including batch_norm variable, therefore we need extra reuse mechnism
		# to reuse batch_norm variables.
		self.weights = self.conv_weights()
		# TODO: meta-test is sort of test stage.
		training = True if mode is 'train' else False

		def meta_task(input):
			"""
			map_fn only support one parameters, so we need to unpack from tuple.
			:param support_x:   [setsz, 84*84*3]
			:param support_y:   [setsz, n-way]
			:param query_x:     [querysz, 84*84*3]
			:param query_y:     [querysz, n-way]
			:param training:    training or not, for batch_norm
			:return:
			"""
			support_x, support_y, query_x, query_y = input
			# to record the op in t update step.
			query_preds, query_losses, query_accs = [], [], []

			# ================================================
			#             True        False         AUTO_REUSE
			# Non-exist   Error       Create one    create one
			# Existed     reuse       Error         reuse
			# ================================================

			support_pred = self.forward(support_x, self.weights, training)
			support_loss = tf.nn.softmax_cross_entropy_with_logits(logits=support_pred, labels=support_y)
			support_acc = tf.contrib.metrics.accuracy(tf.argmax(tf.nn.softmax(support_pred, dim=1), axis=1),
			                                          tf.argmax(support_y, axis=1))

			# compute gradients of reasoning network only
			grads = tf.gradients(support_loss, list(self.weights[1].values()))
			# grad and variable dict
			gvs = dict(zip(self.weights[1].keys(), grads))


			# theta_pi = theta - alpha * grads
			fast_weights_rn = dict(zip(self.weights[1].keys(),
			                        [self.weights[1][key] - self.train_lr * gvs[key] for key in self.weights[1].keys()]))
			# compose fast weigths by replacing with lastest reasoning network
			fast_weights = [self.weights[0], fast_weights_rn]
			# use theta_pi to forward meta-test
			query_pred = self.forward(query_x, fast_weights, training)
			# meta-test loss
			query_loss = tf.nn.softmax_cross_entropy_with_logits(logits=query_pred, labels=query_y)
			# record T0 pred and loss for meta-test
			query_preds.append(query_pred)
			query_losses.append(query_loss)


			# continue to build T1-TK steps graph
			for k in range(1, K):
				# T_k loss on meta-train
				# we need meta-train loss to fine-tune the task and meta-test loss to update theta
				loss = tf.nn.softmax_cross_entropy_with_logits(logits=self.forward(support_x, fast_weights, training),
				                                               labels=support_y)
				# compute gradients of reasoning network only
				grads = tf.gradients(loss, list(fast_weights[1].values()))
				# compose grad and variable dict
				gvs = dict(zip(fast_weights[1].keys(), grads))
				# update theta_pi according to varibles
				fast_weights_rn = dict(zip(fast_weights[1].keys(), [fast_weights[1][key] - self.train_lr * gvs[key]
				                                              for key in fast_weights[1].keys()]))
				# compose fast weigths by replacing with lastest reasoning network
				fast_weights = [self.weights[0], fast_weights_rn]
				# forward on theta_pi
				query_pred = self.forward(query_x, fast_weights, training)
				# we need accumulate all meta-test losses to update theta
				query_loss = tf.nn.softmax_cross_entropy_with_logits(logits=query_pred, labels=query_y)
				query_preds.append(query_pred)
				query_losses.append(query_loss)

			# update moving_mean and moving_variance
			update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
			with tf.control_dependencies(update_ops):
				support_loss = tf.identity(support_loss)


			# compute every steps' accuracy on query set
			for i in range(K):
				query_accs.append(tf.contrib.metrics.accuracy(tf.argmax(tf.nn.softmax(query_preds[i], dim=1), axis=1),
				                                              tf.argmax(query_y, axis=1)))
			# we just use the first step support op: support_pred & support_loss, but igonre these support op
			# at step 1:K-1.
			# however, we return all pred&loss&acc op at each time steps.
			# support_pred: [5, nway]
			# support_loss: [5]
			# support_acc:  scalar
			# query_preds:   [[75, nway]x K ]
			# query_losses:  [[75]x K ]
			# query_accs:    [K]
			result = [support_pred, support_loss, support_acc, query_preds, query_losses, query_accs]

			return result

		# return: [support_pred, support_loss, support_acc, query_preds, query_losses, query_accs]
		out_dtype = [tf.float32, tf.float32, tf.float32, [tf.float32] * K, [tf.float32] * K, [tf.float32] * K]
		result = tf.map_fn(meta_task, elems=(support_xb, support_yb, query_xb, query_yb),
		                   dtype=out_dtype, parallel_iterations=meta_batchsz, name='map_fn')
		support_pred_tasks, support_loss_tasks, support_acc_tasks, \
		query_preds_tasks, query_losses_tasks, query_accs_tasks = result
		# support_pred_tasks:   [4, 5, nway]
		# support_loss_tasks:   [4, 5]
		# support_acc_tasks:    [4]
		# query_preds_tasks:    [4, 75, nway]xK !!!
		# query_losses_tasks:   [4, 75]xK       !!!
		# query_accs_tasks      [4]xK           !!!

		if mode is 'train':
			# [4, 5]
			self.support_loss = support_loss = tf.reduce_sum(support_loss_tasks) / meta_batchsz
			# [4, 75]xK
			self.query_losses = query_losses = [tf.reduce_sum(query_losses_tasks[j]) / meta_batchsz for j in range(K)]
			# [4]
			self.support_acc = support_acc = tf.reduce_sum(support_acc_tasks) / meta_batchsz
			# [4]xK
			self.query_accs = query_accs = [tf.reduce_sum(query_accs_tasks[j]) / meta_batchsz for j in range(K)]


			optimizer = tf.train.AdamOptimizer(self.meta_lr, name='meta_optim', beta1=0)
			# meta-train gradients, query_losses[-1] is the accumulated loss across over tasks.
			# query_loss: mean of [4, 75]
			gvs = optimizer.compute_gradients(self.query_losses[-1])
			# meta-train grads clipping
			# gvs = [(tf.clip_by_value(grad, -10, 10), var) for grad, var in gvs]
			gvs = [(tf.clip_by_norm(grad, 10), var) for grad, var in gvs]
			# update theta
			self.meta_op = optimizer.apply_gradients(gvs)



		else:  # test & eval

			# [4, 5]
			self.test_support_loss = support_loss = tf.reduce_sum(support_loss_tasks) / meta_batchsz
			# [4, 75]xK
			self.test_query_losses = query_losses = [tf.reduce_sum(query_losses_tasks[j]) / meta_batchsz for j in range(K)]
			# [4]
			self.test_support_acc = support_acc = tf.reduce_sum(support_acc_tasks) / meta_batchsz
			# [4]xK
			self.test_query_accs = query_accs = [tf.reduce_sum(query_accs_tasks[j]) / meta_batchsz for j in range(K)]

		# NOTICE: every time build model, support_loss will be added to the summary, but it's different.
		tf.summary.scalar(mode + '：support loss', support_loss)
		tf.summary.scalar(mode + '：support acc', support_acc)
		for j in range(K):
			tf.summary.scalar(mode + '：query loss, step ' + str(j), query_losses[j])
			tf.summary.scalar(mode + '：query acc, step ' + str(j), query_accs[j])



	def conv_weights(self):
		weights = [{}, {}]

		conv_initializer = tf.contrib.layers.xavier_initializer_conv2d()
		fc_initializer = tf.contrib.layers.xavier_initializer()
		k = 3

		with tf.variable_scope('CSML', reuse=tf.AUTO_REUSE):
			weights[0]['conv1'] = tf.get_variable('conv1w', [k, k, 3, self.c], initializer=conv_initializer)
			weights[0]['b1']    = tf.get_variable('conv1b', initializer=tf.zeros([self.c]))
			weights[0]['conv2'] = tf.get_variable('conv2w', [k, k, self.c, self.c], initializer=conv_initializer)
			weights[0]['b2']    = tf.get_variable('conv2b', initializer=tf.zeros([self.c]))
			weights[0]['conv3'] = tf.get_variable('conv3w', [k, k, self.c, self.c], initializer=conv_initializer)
			weights[0]['b3']    = tf.get_variable('conv3b', initializer=tf.zeros([self.c]))
			weights[0]['conv4'] = tf.get_variable('conv4w', [k, k, self.c, self.c], initializer=conv_initializer)
			weights[0]['b4']    = tf.get_variable('conv4b', initializer=tf.zeros([self.c]))


			# relation network
			weights[1]['gw1'] = tf.get_variable('gw1', [self.c, 256], initializer=fc_initializer)
			weights[1]['gb1'] = tf.get_variable('gb1', initializer=tf.zeros([256]))
			weights[1]['gw2'] = tf.get_variable('gw2', [256, 256], initializer=fc_initializer)
			weights[1]['gb2'] = tf.get_variable('gb2', initializer=tf.zeros([256]))
			weights[1]['gw3'] = tf.get_variable('gw3', [256, 256], initializer=fc_initializer)
			weights[1]['gb3'] = tf.get_variable('gb3', initializer=tf.zeros([256]))


			weights[1]['fw1'] = tf.get_variable('fw1', [256, 256], initializer=fc_initializer)
			weights[1]['fb1'] = tf.get_variable('fb1', initializer=tf.zeros([256]))
			weights[1]['fw2'] = tf.get_variable('fw2', [256, 256], initializer=fc_initializer)
			weights[1]['fb2'] = tf.get_variable('fb2', initializer=tf.zeros([256]))
			weights[1]['fw3'] = tf.get_variable('fw3', [256, self.nway], initializer=fc_initializer)
			weights[1]['fb3'] = tf.get_variable('fb3', initializer=tf.zeros([self.nway]))

			return weights

	def conv_block(self, x, weight, bias, scope, training):
		"""
		build a block with conv2d->batch_norm->pooling
		:param x:
		:param weight:
		:param bias:
		:param scope:
		:param training:
		:return:
		"""
		# conv
		x = tf.nn.conv2d(x, weight, [1, 1, 1, 1], 'SAME', name=scope + '_conv2d') + bias
		# batch norm, activation_fn=tf.nn.relu,
		# NOTICE: must have tf.layers.batch_normalization
		# x = tf.contrib.layers.batch_norm(x, activation_fn=tf.nn.relu)
		with tf.variable_scope('CSML'):
			# train is set to True ALWAYS, please refer to https://github.com/cbfinn/maml/issues/9
			# when FLAGS.train=True, we still need to build evaluation network
			x = tf.layers.batch_normalization(x, training=training, name=scope + '_bn', reuse=tf.AUTO_REUSE, fused=True)
		# relu
		x = tf.nn.relu(x, name=scope + '_relu')
		# pooling
		x = tf.nn.max_pool(x, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', name=scope + '_pool')
		return x

	def fc_block(self, x, weight, bias, scope, training, activation=True):
		"""

		:param x:
		:param weight:
		:param bias:
		:param scope:
		:param training:
		:param activation:
		:return:
		"""
		with tf.name_scope(scope):
			x = tf.nn.xw_plus_b(x, weight, bias, name='fc')
			if activation:
				# x = tf.nn.dropout(x, 0.5, name='dropout')
				x = tf.nn.relu(x, name='relu')

		return x

	def forward2(self, x, weights, training):
		"""
		:param x:
		:param weights:
		:param training:
		:return:
		"""
		# [b, 84, 84, 3]
		x = tf.reshape(x, [-1, self.imgsz, self.imgsz, self.c_], name='reshape1')

		x = self.conv_block(x, weights[0]['conv1'], weights[0]['b1'], 'conv0', training)
		x = self.conv_block(x, weights[0]['conv2'], weights[0]['b2'], 'conv1', training)
		x = self.conv_block(x, weights[0]['conv3'], weights[0]['b3'], 'conv2', training)
		x = self.conv_block(x, weights[0]['conv4'], weights[0]['b4'], 'conv3', training)

		# get_shape is static shape, (5, 5, 5, 32)
		# hidden4 = tf.reshape(x, [-1, np.prod([int(dim) for dim in hidden4.get_shape()[1:]])], name='reshape2')
		x = tf.layers.flatten(x, name='flatten')

		x = self.fc_block(x, weights[1]['w5'], weights[1]['b5'], scope='fc1', training=training, activation=False)
		# x = self.fc_block(x, weights[1]['fc2w'], weights[1]['fc2b'], scope='fc2', training=training, activation=False)


		return x

	def forward(self, x, weights, training):
		"""


		:param x:
		:param weights:
		:param training:
		:return:
		"""
		# [b, 84, 84, 3]
		x = tf.reshape(x, [-1, self.imgsz, self.imgsz, self.imgc])
		x = self.conv_block(x, weights[0]['conv1'], weights[0]['b1'], 'conv0', training)
		x = self.conv_block(x, weights[0]['conv2'], weights[0]['b2'], 'conv1', training)
		x = self.conv_block(x, weights[0]['conv3'], weights[0]['b3'], 'conv2', training)
		x = self.conv_block(x, weights[0]['conv4'], weights[0]['b4'], 'conv3', training)


		# x: [setsz, 5, 5, 32] => [setsz, 5*5, 256]
		x = tf.reshape(x, (-1, self.c))
		x = self.fc_block(x, weights[1]['gw1'], weights[1]['gb1'], 'g1', training, activation=True)
		x = self.fc_block(x, weights[1]['gw2'], weights[1]['gb2'], 'g2', training, activation=True)
		x = self.fc_block(x, weights[1]['gw3'], weights[1]['gb3'], 'g3', training, activation=True)
		x = tf.reshape(x, (-1, 5 * 5, 256))

		# sum up
		# [setsz, 5*5, g_c] => [setsz, g_c]
		x = tf.reduce_sum(x, axis=1)

		# x: [setsz, g_c] => [setsz, nway]
		x = self.fc_block(x, weights[1]['fw1'], weights[1]['fb1'], 'f1', training, activation=True)
		x = self.fc_block(x, weights[1]['fw2'], weights[1]['fb2'], 'f2', training, activation=True)
		pred = self.fc_block(x, weights[1]['fw3'], weights[1]['fb3'], 'f3', training, activation=False)


		return pred